#! /bin/bash
ASKED="$1" # must be "right", "up", "left" or "down"
ACTIVE="$(xdotool getactivewindow)"

get_window_geometry() {
    # requires: $1 is the xdotool code for that window
    # returns (echoes): "x y width height screen"
    GEOMETRY="$(xdotool getwindowgeometry "$1")"

    # everything should be done with a single call to sed!
    X="$(echo "$GEOMETRY"|
        grep "Position"|
        sed "s/Position: \([0-9\-]*\),\([0-9\-]*\) (screen: \([0-9\-]*\))/\1/")"
    Y="$(echo "$GEOMETRY"|
        grep "Position"|
        sed "s/Position: \([0-9\-]*\),\([0-9\-]*\) (screen: \([0-9\-]*\))/\2/")"
    SCREEN="$(echo "$GEOMETRY"|
        grep "Position"|
        sed "s/Position: \([0-9\-]*\),\([0-9\-]*\) (screen: \([0-9\-]*\))/\3/")"

    WIDTH="$(echo "$GEOMETRY"|
        grep "Geometry"|
        sed "s/Geometry: \([0-9]*\)x[0-9]*/\1/")"
    HEIGHT="$(echo "$GEOMETRY"|
        grep "Geometry"|
        sed "s/Geometry: [0-9]*x\([0-9]*\)/\1/")"

    echo "$X $Y $WIDTH $HEIGHT $SCREEN"
}

get_window_center() {
    # requires: $1 is a xdotool geometry, as produced by get_window_geometry
    # returns (echoes): "x y" the coordinates of the center of the window
    # http://superuser.com/questions/121627
    # /how-to-get-elements-from-list-in-bash
    INFOS=($1)
    X=$((${INFOS[0]} + ${INFOS[2]} / 2))
    Y=$((${INFOS[1]} + ${INFOS[3]} / 2))
    echo "$X $Y"
}

get_relative_position() {
    # requires: $1 is the "x y" coordinates of a point A
    # requires: $2 is the "x y" coordinates of a point B
    # returns (echoes): STRING in `B is located at the STRING of A`
    # where WORD is "right", "left", "up" or "down"
    A=($1)
    B=($2)
    C[0]="$((${B[0]} - ${A[0]}))"
    C[1]="$((${B[1]} - ${A[1]}))"
    if [ "$((${C[0]} + ${C[1]}))" -lt "0" ]; then
        if [ "$((${C[0]} - ${C[1]}))" -lt "0" ]; then
            echo "left"
        else
            echo "up"
        fi
    else
        if [ "$((${C[0]} - ${C[1]}))" -lt "0" ]; then
            echo "down"
        else
            echo "right"
        fi
    fi
}

get_window_name() {
    # requires: $1 is the xdotool code for a window
    CODE="$1"
    xdotool getwindowname "$CODE"
}

get_windows_by_location() {
    # requires: $1 is either "right", "left", "up" or "down"
    # requires: $2 is the xdotool code for a window
    WANTEDPOS="$1"
    ACTIVE="$2"
    ACTIVEGEOMETRY=$(get_window_geometry "$ACTIVE")
    ACTIVEGEOMETRY_A=($ACTIVEGEOMETRY)
    ACTIVECENTER="$(get_window_center "$ACTIVEGEOMETRY")"
    ACTIVEDESKTOP="$(xdotool get_desktop_for_window "$ACTIVE")"

    RESULTS="" # to debug by displaying what are the candidates

    CANDIDATEFOUND=false
    BESTCANDIDATE=""
    BESTDISTANCE=""

    for line in $(xdotool search --onlyvisible \
                                 --screen ${ACTIVEGEOMETRY_A[4]} \
                                 --name ".+"); do
        CURRENTGEOMETRY="$(get_window_geometry "$line")"
        CURRENTCENTER="$(get_window_center "$CURRENTGEOMETRY")"

        if [ "$line" != "$ACTIVE" -a "$(get_relative_position "$ACTIVECENTER" "$CURRENTCENTER")" == "$WANTEDPOS" ]; then

            NAME="$(get_window_name "$line")"

            if is_valid_window "$ACTIVE" "$ACTIVEDESKTOP" "$ACTIVEGEOMETRY" "$line" "$NAME" "$CURRENTGEOMETRY"; then

                RESULTS="$RESULTS\n$line -> $NAME"

                if [ "$CANDIDATEFOUND" == "false" ]; then
                    CANDIDATEFOUND=true
                    BESTCANDIDATE="$line"
                    BESTDISTANCE="$(windows_distance "$CURRENTGEOMETRY" "$ACTIVEGEOMETRY")"
                else
                    NEWDISTANCE="$(windows_distance "$CURRENTGEOMETRY" "$ACTIVEGEOMETRY")"
                    if [ "$NEWDISTANCE" -lt "$BESTDISTANCE" ]; then
                        BESTCANDIDATE="$line"
                        BESTDISTANCE="$NEWDISTANCE"
                    fi
                fi

            fi

        fi
    done

    # echo -e "$RESULTS" # to debug

    echo "$BESTCANDIDATE"
}

is_valid_window() {
    # requires $1 is the active windows code
    # requires $2 is the active windows desktop
    # requires $3 is the active windows geometry
    # requires $4 is the candidate windows code
    # requires $5 is the candidate windows name
    # requires $6 is the candidate windows geometry
    # return sucess (0) if the window is valid, error (1) otherwise
    ACTIVE="$1"
    ACTIVEDESKTOP="$2"
    ACTIVEGEOMETRY="$3"
    CANDIDATE="$4"
    CANDIDATENAME="$5"
    CANDIDATEGEOMETRY="$6"
    if name_is_blacklisted "$CANDIDATENAME"; then
        return 1
    fi
    CANDIDATEDESKTOP="$(xdotool get_desktop_for_window "$CANDIDATE" 2> /dev/null)"
    RETURNED="$?"
    # replace '!= "$ACTIVEDESKTOP"' by
    #         '== "-1"' to enable jumping to other desktop
    # I did not test that
    if [ "$RETURNED" != "0" -o "$CANDIDATEDESKTOP" != "$ACTIVEDESKTOP" ]; then
        return 1
    fi
    if geometries_intersect "$ACTIVEGEOMETRY" "$CANDIDATEGEOMETRY"; then
        return 1
    fi
    return 0
}

name_is_blacklisted() {
    # requires: $1 is a name to test for blacklisting
    # return true if blacklisted, false if not
    NAME="$1"
    case "$NAME" in
        gnome-shell|hp-systray|"mutter guard window")
            return 0
    esac
    return 1
    }

geometries_intersect() {
    # requires: $1 is a geometry as returned by get_window_geometry
    # requires: $2 is a geometry as returned by get_window_geometry
    GEOMETRY_1_A=($1)
    GEOMETRY_2_A=($2)
    LEFT_1="${GEOMETRY_1_A[0]}"
    LEFT_2="${GEOMETRY_2_A[0]}"
    RIGHT_1="$((LEFT_1 + ${GEOMETRY_1_A[2]}))"
    RIGHT_2="$((LEFT_2 + ${GEOMETRY_2_A[2]}))"
    UP_1="${GEOMETRY_1_A[1]}"
    UP_2="${GEOMETRY_2_A[1]}"
    LOW_1="$((UP_1 + ${GEOMETRY_1_A[3]}))"
    LOW_2="$((UP_2 + ${GEOMETRY_2_A[3]}))"
    if segments_intersect "$LEFT_1" "$RIGHT_1" "$LEFT_2" "$RIGHT_2" &&
        segments_intersect "$UP_1" "$LOW_1" "$UP_2" "$LOW_2"; then
        return 0
    fi
    return 1
}

segments_intersect() {
    # requires: $1 start of segment A
    # requires: $2 end of segment A
    # with $1 <= $2
    # requires: $3 start of segment B
    # requires: $4 end of segment B
    # with $3 <= $4
    A_START="$1"
    A_END="$2"
    B_START="$3"
    B_END="$4"

    if [ "$A_END" -lt "$B_START" -o "$B_END" -lt "$A_START" ]; then
        return 1
    fi

    return 0
}

points_distance() {
    # requires: $1 is the "x y" coordinates of a point A
    # requires: $2 is the "x y" coordinates of a point B
    # returns (echoes) the square of the length (norm2) of segment AB
    A=($1)
    B=($2)
    bc <<< "(${A[0]} - ${B[0]})^2 + \
        (${A[1]} - ${B[1]})^2"
}

windows_distance() {
    # requires: $1 is a geometry as returned by get_window_geometry
    # requires: $2 is a geometry as returned by get_window_geometry
    # returns (echoes) the square of the distance (norm2)
    # from one center to the other
    # (choosing center is not optimal for user experience,
    # it could be improved: this is a demo)
    CENTER1="$(get_window_center "$1")"
    CENTER2="$(get_window_center "$2")"
    points_distance "$CENTER1" "$CENTER2"
}


# read the difference between windowactivate and windowfocus in man
xdotool windowactivate "$(get_windows_by_location "$ASKED" "$ACTIVE")"
